const fs = require('fs');
const path = require('path');
const geoip = require('geoip-lite');
const { parse } = require('nginx-access-log')

//const interval = 10*1000;
//(execute continuously)

const interval = null;
//(execute once, e.g. with crontab)

const accessLogPath = "/var/log/nginx/access.log";
const siteRootDir = "/var/www/nikky";
const statFileName = "_stats.txt";
const allowedFileTypes = ['.html', '.zip', '.exe', '.apk'];
const maxLastViews = 10;
const ipHashSeed = 543534453;

let lastChecked;
if(interval){
	lastChecked = new Date();
	setInterval(() => {
		update();
		lastChecked = new Date();
	}, interval);
}else{
	const tmpPath = __dirname+'/lastChecked';
	lastChecked = fs.existsSync(tmpPath) ? fs.statSync(tmpPath)?.mtime : new Date();
	fs.closeSync(fs.openSync(tmpPath, 'w'));

	update();
}

function update(){
	const logData = fs.readFileSync(accessLogPath, "utf-8");
	const logs = parse(logData);
	logs.forEach(log => {
		if(log.time < lastChecked) return;
		if(log.status >= 400) return;
		if(log.uri.length > 2048) return;
		if(log.referer.length > 2048) return;
		if(log.host == '127.0.0.1') return;
		let filePath = siteRootDir + log.uri;
		if(log.uri.length && (log.uri[log.uri.length-1] == '/')) filePath += 'index.html';
		if(!allowedFileTypes.includes(path.extname(filePath))) return;
		const fileName = path.basename(filePath);
		const statsPath = path.dirname(filePath)+'/'+statFileName;
		//console.log(log);
		fs.readFile(statsPath, {encoding: 'utf-8'}, (err, data) => {
			if(err) return;
			const currentFileStats = {
				views: 0,
				last: []
			};
			let lines = data.split("\n");
			let currentStartsAt, currentEndsAt;
			let currentFound = false;
			let parsingLast = false;
			for(var lineNum=0; lineNum < lines.length; lineNum++){
				const line = lines[lineNum];
				if(line[0] == '@'){
					parsingLast = false;
					if(currentFound){
						currentEndsAt = lineNum;
						break;
					}else if(line == '@'+fileName){
						currentFound = true;
						currentStartsAt = lineNum;
					}
				}else if(currentFound){
					if(parsingLast){
						const match = line.match(/^(.+) from (.+) at (.+?)(?:\s*referer: (.+))?$/);
						if(match){
							currentFileStats.last.push({
								ip: match[1],
								from: match[2],
								time: match[3],
								referer: match[4]
							});
						}
					}else{
						const match = line.match(/^(.*?):\s+?(.*)$/);
						if(match){
							switch(match[1]){
								case 'views':
									currentFileStats.views = parseInt(match[2]);
									break;
								case 'last':
									parsingLast = true;
									break;
							}
						}
					}
				}
			}
			if(!currentEndsAt) currentEndsAt = lines.length;

			const hashedIp = ipHashSeed ? cyrb53(log.host, ipHashSeed).toString() : log.host;
			if(currentFileStats.last.some(e => e.ip === hashedIp)){
				//same ip as one of last visits, skipping
				return;
			}else{
				const geo = geoip.lookup(log.host);
				currentFileStats.last.push({
					ip: hashedIp,
					time: log.time.toUTCString(),
					from: geo ? geo.country+(geo.city ? ', '+geo.city : '') : '-',
					referer: (log.referer && (log.referer !== '-')) ? log.referer.replace(/[^\x20-\x7F]/g, "?") : null
				});
				currentFileStats.views++;
			}
			if(currentFileStats.last.length > maxLastViews){
				currentFileStats.last.splice(0,1);
			}
			
			const newLines = [
				"@"+fileName,
				'views: '+currentFileStats.views,
				'last: ',
				...currentFileStats.last.map(e => `${e.ip} from ${e.from} at ${e.time}${e.referer ? ' referer: '+e.referer : ''}`),
				''
			];
			if(currentFound){
				lines.splice(currentStartsAt, currentEndsAt - currentStartsAt, ...newLines);
			}else{
				lines = lines.concat(newLines);
			}
			fs.writeFile(statsPath, lines.join("\n"), (err) => { if(err) console.error(err); });
		});
	});
}

//https://stackoverflow.com/a/52171480/13652003
const cyrb53 = function(str, seed = 0) {
    let h1 = 0xdeadbeef ^ seed, h2 = 0x41c6ce57 ^ seed;
    for (let i = 0, ch; i < str.length; i++) {
        ch = str.charCodeAt(i);
        h1 = Math.imul(h1 ^ ch, 2654435761);
        h2 = Math.imul(h2 ^ ch, 1597334677);
    }
    h1 = Math.imul(h1 ^ (h1>>>16), 2246822507) ^ Math.imul(h2 ^ (h2>>>13), 3266489909);
    h2 = Math.imul(h2 ^ (h2>>>16), 2246822507) ^ Math.imul(h1 ^ (h1>>>13), 3266489909);
    return 4294967296 * (2097151 & h2) + (h1>>>0);
};
