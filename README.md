# Simple stat logger

## Purpose

This script reads nginx access logs for very barebones analytics.

It writes to a `_stats.txt` file in a directory that already has it.

## Usage

* `npm install`
* Set up `/etc/nginx/nginx.conf` [like this](https://github.com/onyanko-pon/nginx-access-log#nginx-log-format)
* Modify path variables inside `index.js`
* Create empty `_stats.txt` files inside folders where you want them to be
* Create a crontab job to run the script every minute by typing `crontab -e` and adding a line
```
*/1 * * * * node /path/to/script/index.js
```

## Limitations

* Due to a bug in the parser library, it's currently required to use UTC+0 timezone.
* The stats files are stored publicly, in the same folder where the accessed file is located.

## Example output

```
@index.html
views: 5
last: 
2322855228853034 from US, San Antonio at Tue, 14 Sep 2021 00:44:43 GMT
1040892493084384 from UK at Tue, 14 Sep 2021 09:09:43 GMT
9599220393244052 from KZ, Nur-Sultan at Tue, 14 Sep 2021 18:29:30 GMT
9401105390322343 from RU, Krasnodar at Tue, 14 Sep 2021 20:11:45 GMT

@game.html
views: 1
last: 
2322855228853034 from US, San Antonio at Tue, 14 Sep 2021 14:31:59 GMT
```
